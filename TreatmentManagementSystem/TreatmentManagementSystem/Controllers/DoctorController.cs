﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TMS.Data.Data;
using TMS.Data.Data.Models;
using TreatmentManagementSystem.ViewModels;

namespace TreatmentManagementSystem.Controllers
{
    public class DoctorController : Controller
    {
        private readonly TmsDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        CreateProfileViewModel createVm = new CreateProfileViewModel();
        ContentViewModel contentVm = new ContentViewModel();
        MessageViewModel messageVm = new MessageViewModel();

        public DoctorController(TmsDbContext context, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region Actions 
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public ActionResult Index(bool isHighAttention, string searchString)
        {
            var vm = this.GetDoctorContent();
            if (!String.IsNullOrEmpty(searchString))
            {
                vm.Patients = vm.Patients.Where(s => s.Name.Contains(searchString));
                
            }
            if (isHighAttention)
            {
                vm.Patients = vm.Patients.Where(s => s.WithHighAttention == true);
            }
            return View(vm);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public IActionResult CreatePatient()
        {
            var doctor = this.GetCurrentDoctor();
            var users = _userManager.Users;
            if (doctor != null && (users != null && users.Count() > 0))
            {
                createVm.AppUsers = users;
            }
            return View(createVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<ActionResult> CreatePatient(CreateProfileViewModel model)
        {
            var patient = new Patient();
            var users = _userManager.Users;
            Guid newPatientID = Guid.NewGuid();
            var doctor = this.GetCurrentDoctor();

            if (!string.IsNullOrWhiteSpace(model.Input.UserId))
            {
                var user = users != null ? users.Where(u => u.Id == model.Input.UserId).FirstOrDefault() : null;
                if (ModelState.IsValid && user != null)
                {
                    patient.Id = newPatientID;
                    patient.Name = model.Input.Name;
                    patient.Email = model.Input.Email;
                    patient.Notes = model.Input.Notes;
                    patient.WithHighAttention = model.Input.WithHighAttention;
                    user.ProfileID = newPatientID;
                    patient.DoctorID = doctor.Id;
                    _context.Patients.Add(patient);
                    await _userManager.UpdateAsync(user);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return View(patient);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var patient = await _context.Patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }
            return View(patient);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> Edit(Guid id, Patient patient)
        {
            if (id != patient.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Patients.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(patient);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            if (patient != null)
            {
                contentVm.Id = patient.Id;
                contentVm.Name = patient.Name;
                contentVm.Email = patient.Email;
                contentVm.Notes = patient.Notes;
            }
            return View(contentVm);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            if (patient != null)
            {
                contentVm.Id = patient.Id;
                contentVm.Name = patient.Name;
                contentVm.Email = patient.Email;
            }
            return View(contentVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var patient = await _context.Patients.FindAsync(id);
            var user = _userManager.Users.Where(u => u.ProfileID == patient.Id)?.FirstOrDefault();            

            if (patient != null && user != null)
            {
                try
                {
                    user.ProfileID = Guid.Empty;
                    await _userManager.UpdateAsync(user);

                    _context.Patients.Remove(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region MessageBoard Actions 

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public ActionResult MessageBoard()
        {
            var vm = this.GetMessageBoardContent();
            return View(vm);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public ActionResult Inbox()
        {
            var vm = this.GetMessageBoardContent();
            return View(vm);
        }

        [Authorize]
        [Authorize(Roles = "Doctor")]
        public IActionResult CreateMessage()
        {
            var doctor = this.GetCurrentDoctor();
            var patients = _context.Patients;
            if (doctor != null && (patients != null && patients.Count() > 0))
            {
                messageVm.Patients = patients.Where(p => p.DoctorID == doctor.Id);
            }
            return View(messageVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<ActionResult> CreateMessage(MessageViewModel model)
        {
            var message = new Message();

            if (model.Input.PatientID != Guid.Empty)
            {
                var doctor = this.GetCurrentDoctor();
                var patients = _context.Patients;
                if (ModelState.IsValid && doctor != null)
                {
                    message.Id = Guid.NewGuid();
                    message.Subject = model.Input.Subject;
                    message.Text = model.Input.Text;
                    message.IsRead = false;
                    message.DoctorID = doctor.Id;
                    message.SenderID = doctor.Id;
                    message.PatientID = model.Input.PatientID;
                    message.TimeStamp = DateTime.Now;
                    _context.Messages.Add(message);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("MessageBoard");
                }
            }
            return View(message);
        }

        [Authorize(Roles = "Doctor")]
        public  IActionResult Reply(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            if (id != Guid.Empty)
            {
                messageVm.PatientID = id;
            }

            return View("ReplyMessage",messageVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<ActionResult> ReplyMessage(MessageViewModel model)
        {
            var message = new Message();
            
                var doctor = this.GetCurrentDoctor();
                if (ModelState.IsValid && doctor != null)
                {
                    message.Id = Guid.NewGuid();
                    message.Subject = model.Input.Subject;
                    message.Text = model.Input.Text;
                    message.IsRead = false;
                    message.DoctorID = doctor.Id;
                    message.SenderID = doctor.Id;
                    message.PatientID = model.PatientID;
                    message.TimeStamp = DateTime.Now;
                     _context.Messages.Add(message);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("MessageBoard");
                }
           
            return View(message);
        }

        [HttpPost, ActionName("EditMessage")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> EditMessage(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var message = await _context.Messages.FindAsync(id);
            if (message != null)
            {
                message.IsRead = true;
                try
                {
                    _context.Messages.Update(message);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MessageExists(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MessageBoard));                
            }
            return View();
        }

        [HttpPost, ActionName("DeleteMessage")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Doctor")]
        public async Task<IActionResult> DeleteMessage(Guid id)
        {
            var message = await _context.Messages.FindAsync(id);
            
            if (message != null)
            {
                try
                {
                    _context.Messages.Remove(message);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MessageExists(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Inbox));
            }
            return View();
        }

        #endregion

        #region API Calls

        //not used
        [HttpGet]
        [Authorize(Roles = "Doctor")]
        public ActionResult GetAll()
        {
            return Json(new { data = GetDoctorContent().Patients });
        }
        #endregion

        #region Private methods
        private ContentViewModel GetDoctorContent()
        {
            var doctor = this.GetCurrentDoctor();
            var patients = _context.Patients;
            if (doctor != null)
            {
                contentVm.Name = doctor.Name;
                contentVm.Email = doctor.Email;
                contentVm.Patients = patients != null && patients.Count() > 0 ? patients.Where(p => p.DoctorID == doctor.Id) : null;
                contentVm.MessageCount = GetMessageBoardContent().MessageCount;
            }
            return contentVm;
        }

        private Doctor GetCurrentDoctor()
        {
            var currentUserId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var users = _userManager.Users;
            var user = users != null ? users.Where(u => u.Id == currentUserId).FirstOrDefault() : null;
            var doctors = _context.Doctors;
            var doctor = doctors != null && doctors.Count() > 0 ? doctors.Where(d => d.Id == user.ProfileID).FirstOrDefault() : null;

            return doctor;
        }

        private bool PatientExists(Guid id)
        {
            return _context.Patients.Any(e => e.Id == id);
        }

        private bool MessageExists(Guid id)
        {
            return _context.Messages.Any(e => e.Id == id);
        }

        private MessageViewModel GetMessageBoardContent()
        {
            var doctor = GetCurrentDoctor();
            var messages = _context.Messages;
            if (doctor != null)
            {
                var tmsMemberMessages = messages != null && messages.Count() > 0 ? messages.Where(p => p.DoctorID == doctor.Id && p.SenderID != doctor.Id) : null;
                messageVm.Messages = tmsMemberMessages;
                messageVm.MemberName = doctor.Name;
                messageVm.Patients = _context.Patients;
                messageVm.MessageCount = tmsMemberMessages != null && tmsMemberMessages.Count() > 0 ? tmsMemberMessages.Where(m => m.IsRead == false).Count() : 0;
                //vm.SenderID = 
            }
            return messageVm;
        }
        #endregion
    }
}

