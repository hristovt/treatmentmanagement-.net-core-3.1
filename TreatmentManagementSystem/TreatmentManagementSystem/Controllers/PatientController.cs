﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TMS.Data.Data;
using TMS.Data.Data.Models;
using TreatmentManagementSystem.ViewModels;

namespace TreatmentManagementSystem.Controllers
{
    public class PatientController : Controller
    {
        private readonly TmsDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        ContentViewModel contentVm = new ContentViewModel();
        MessageViewModel messageVm = new MessageViewModel();
        public PatientController(TmsDbContext context, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region Actions 
        [Authorize]
        [Authorize(Roles = "Patient")]
        public IActionResult Index()
        {
            var vm = this.GetPatientContent();
            return View(vm);
        }

        #endregion

        #region MessageBoard Actions

        [Authorize(Roles = "Patient")]
        public ActionResult MessageBoard()
        {
            var vm = this.GetMessageBoardContent();
            return View(vm);
        }

        public ActionResult Inbox()
        {
            var vm = this.GetMessageBoardContent();
            return View(vm);
        }

        [Authorize(Roles = "Patient")]
        public IActionResult CreateMessage()
        {
            return View(messageVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Patient")]
        public async Task<ActionResult> CreateMessage(MessageViewModel model)
        {
            var message = new Message();

            var patient = this.GetCurrentPatient();
            if (ModelState.IsValid && patient != null)
            {
                message.Id = Guid.NewGuid();
                message.Subject = model.Input.Subject;
                message.Text = model.Input.Text;
                message.IsRead = false;
                message.DoctorID = patient.DoctorID;
                message.SenderID = patient.Id;
                message.PatientID = patient.Id;
                message.TimeStamp = DateTime.Now;
                _context.Messages.Add(message);
                await _context.SaveChangesAsync();
                return RedirectToAction("MessageBoard");
            }
            return View(message);
        }

        [Authorize(Roles = "Patient")]
        public IActionResult Reply(Guid id)
        {
            MessageViewModel messageVm = new MessageViewModel();
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            if (id != Guid.Empty)
            {
                messageVm.DoctorID = id;
            }

            return View("ReplyMessage", messageVm);
        }       

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Patient")]
        public async Task<ActionResult> ReplyMessage(MessageViewModel model)
        {
            var message = new Message();

            var patient = this.GetCurrentPatient();
            if (ModelState.IsValid && patient != null)
            {
                message.Id = Guid.NewGuid();
                message.Subject = model.Input.Subject;
                message.Text = model.Input.Text;
                message.IsRead = false;
                message.DoctorID = model.DoctorID;
                message.SenderID = patient.Id;
                message.PatientID = patient.Id;
                message.TimeStamp = DateTime.Now;
                _context.Messages.Add(message);
                await _context.SaveChangesAsync();
                return RedirectToAction("MessageBoard");
            }

            return View(message);
        }

        [HttpPost, ActionName("EditMessage")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Patient")]
        public async Task<IActionResult> EditMessage(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var message = await _context.Messages.FindAsync(id);
            if (message != null)
            {
                message.IsRead = true;
                try
                {
                    _context.Messages.Update(message);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MessageExists(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MessageBoard));
            }
            return View();
        }

        [HttpPost, ActionName("DeleteMessage")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Patient")]
        public async Task<IActionResult> DeleteMessage(Guid id)
        {
            var message = await _context.Messages.FindAsync(id);

            if (message != null)
            {
                try
                {
                    _context.Messages.Remove(message);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MessageExists(message.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Inbox));
            }
            return View();
        }

        #endregion

        #region Private methods
        private ContentViewModel GetPatientContent()
        {
            var patient = this.GetCurrentPatient();
            if (patient != null)
            {
                contentVm.Name = patient.Name;
                contentVm.Email = patient.Email;
                contentVm.MessageCount = GetMessageBoardContent().MessageCount;
            }
            return contentVm;
        }

        private Patient GetCurrentPatient()
        {
            var currentUserId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var users = _userManager.Users;
            var user = users != null ? users.Where(u => u.Id == currentUserId).FirstOrDefault() : null;
            var patients = _context.Patients;
            var patient = patients != null && patients.Count() > 0 ? patients.Where(d => d.Id == user.ProfileID).FirstOrDefault() : null;

            return patient;
        }

        private bool MessageExists(Guid id)
        {
            return _context.Messages.Any(e => e.Id == id);
        }
        private MessageViewModel GetMessageBoardContent()
        {
            var patient = GetCurrentPatient();
            var messages = _context.Messages;
            if (patient != null)
            {
                var tmsMemberMessages = messages != null && messages.Count() > 0 ? messages.Where(p => p.PatientID == patient.Id && p.SenderID != patient.Id) : null;
                messageVm.Messages = tmsMemberMessages;
                messageVm.MemberName = patient.Name;
                messageVm.Doctors = _context.Doctors;
                messageVm.MessageCount = tmsMemberMessages != null && tmsMemberMessages.Count() > 0 ? tmsMemberMessages.Where(m => m.IsRead == false).Count() : 0;
            }
            return messageVm;
        }
        #endregion
    }
}
