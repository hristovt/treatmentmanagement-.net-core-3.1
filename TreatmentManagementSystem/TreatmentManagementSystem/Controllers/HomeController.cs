﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TreatmentManagementSystem.Models;

namespace TreatmentManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Profile()
        {
            if (User.IsInRole("Developer"))
            {
                return Redirect("~/Maintenance/Index");
            }
            else if (User.IsInRole("Administrator"))
            {
               return Redirect("~/Administrator/Index");
            }
            else if (User.IsInRole("Doctor"))
            {
               return Redirect("~/Doctor/Index");
            }
            else if (User.IsInRole("Patient"))
            {
                return Redirect("~/Patient/Index");
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
