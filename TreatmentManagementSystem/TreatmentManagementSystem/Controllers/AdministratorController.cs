﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TMS.Data.Data;
using TMS.Data.Data.Models;
using TreatmentManagementSystem.ViewModels;

namespace TreatmentManagementSystem.Controllers
{
    public class AdministratorController : Controller
    {
        private readonly TmsDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        CreateProfileViewModel createVm = new CreateProfileViewModel();
        ContentViewModel contentVm = new ContentViewModel();

        public AdministratorController(TmsDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region Actions 
        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public ActionResult Index()
        {
            contentVm = this.GetAdministratorContent();
            return View(contentVm);
        }
        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public IActionResult CreateDoctor()
        {
            var users = _userManager.Users;
            createVm.AppUsers = users != null && users.Count() > 0 ? users : null;
            return View(createVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<ActionResult> CreateDoctor(CreateProfileViewModel item)
        {
            var doctor = new Doctor();
            var users = _userManager.Users;
            Guid doctorID = Guid.NewGuid();

            if (!string.IsNullOrWhiteSpace(item.Input.UserId) && !string.IsNullOrWhiteSpace(item.Input.Name) && !string.IsNullOrWhiteSpace(item.Input.Email))
            {
                var user = users != null ? users.Where(u => u.Id == item.Input.UserId).FirstOrDefault() : null;
                if (ModelState.IsValid && user != null)
                {
                    doctor.Id = doctorID;
                    doctor.Name = item.Input.Name;
                    doctor.Email = item.Input.Email;
                    user.ProfileID = doctorID;
                    _context.Doctors.Add(doctor);
                    await _userManager.UpdateAsync(user);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("CreateDoctor");
            }
            return View(doctor);
        }

        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var doctor = await _context.Doctors.FindAsync(id);
            if (doctor == null)
            {
                return NotFound();
            }

            return View(doctor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,Email")] Doctor doctor)
        {
            if (id != doctor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(doctor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DoctorExists(doctor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(doctor);
        }

        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var doctor = await _context.Doctors
                .FirstOrDefaultAsync(m => m.Id == id);
            if (doctor == null)
            {
                return NotFound();
            }

            if (doctor != null)
            {
                contentVm.Id = doctor.Id;
                contentVm.Name = doctor.Name;
                contentVm.Email = doctor.Email;
            }

            return View(contentVm);
        }

        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var doctor = await _context.Doctors
                .FirstOrDefaultAsync(m => m.Id == id);
            if (doctor == null)
            {
                return NotFound();
            }

            if (doctor != null)
            {
                contentVm.Id = doctor.Id;
                contentVm.Name = doctor.Name;
                contentVm.Email = doctor.Email;
            }

            return View(contentVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Developer, Administrator")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var doctor = await _context.Doctors.FindAsync(id);
            var user = _userManager.Users.Where(u => u.ProfileID == doctor.Id)?.FirstOrDefault();

            if (doctor != null && user != null)
            {
                try
                {
                user.ProfileID = Guid.Empty;
                await _userManager.UpdateAsync(user);

                _context.Doctors.Remove(doctor);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoctorExists(doctor.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }
            return RedirectToAction(nameof(Index));
        }
        #endregion  

        #region Private methods
        private ContentViewModel GetMaintenanceContent()
        {
            var admins = _context.Admins;

            contentVm.Admins = admins != null && admins.Count() > 0 ? admins : null;

            return contentVm;
        }

        private ContentViewModel GetAdministratorContent()
        {           
            var admin = this.GetCurrentAdministrator();
            if (admin != null)
            {
                contentVm.Name = admin.Name;
                contentVm.Email = admin.Email;
                contentVm.Doctors = _context.Doctors;
            }
            return contentVm;
        }

        private Admin GetCurrentAdministrator()
        {
            var currentUserId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var users = _userManager.Users;
            var user = users != null ? users.Where(u => u.Id == currentUserId).FirstOrDefault() : null;
            var admins = _context.Admins;
            var admin = admins != null && admins.Count() > 0 ? admins.Where(a => a.Id == user.ProfileID).FirstOrDefault() : null;

            return admin;
        }

        private bool DoctorExists(Guid id)
        {
            return _context.Doctors.Any(e => e.Id == id);
        }
        #endregion
    }
}
