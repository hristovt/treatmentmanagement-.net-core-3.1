﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TreatmentManagementSystem.ViewModels;
using TMS.Data.Data;
using TMS.Data.Data.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using TreatmentManagementSystem.Core.Helpers;

namespace TreatmentManagementSystem.Controllers
{
    public class MaintenanceController : Controller
    {
        private readonly TmsDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        CreateProfileViewModel createVm = new CreateProfileViewModel();
        ContentViewModel contentVm = new ContentViewModel();
        public MaintenanceController(TmsDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region Actions         
        [Authorize(Roles = "Developer")]
        public IActionResult Index()
        {
            return View(this.GetMaintenanceContent());
        }      

        [Authorize(Roles = "Developer")]
        public IActionResult CreateAdmin()
        {
            var users = _userManager.Users;
            if (users != null && users.Count() > 0)
            {
                createVm.AppUsers = users;
            }
            return View(createVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Developer")]
        public async Task<ActionResult> CreateAdmin(CreateProfileViewModel item)
        {
            var admin = new Admin();
            var users = _userManager.Users;
            Guid adminID = Guid.NewGuid();

            if (!string.IsNullOrWhiteSpace(item.Input.UserId) && !string.IsNullOrWhiteSpace(item.Input.Name) && !string.IsNullOrWhiteSpace(item.Input.Email))
            {
                var user = users != null ? users.Where(u => u.Id == item.Input.UserId).FirstOrDefault() : null;
                if (ModelState.IsValid && user != null)
                {
                    admin.Id = adminID;
                    admin.Name = item.Input.Name;
                    admin.Email = item.Input.Email;
                    user.ProfileID = adminID;
                    _context.Admins.Add(admin);
                    await _userManager.UpdateAsync(user);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("CreateAdmin");
            }
            return View(admin);
        }

        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var admin = await _context.Admins.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }
            return View(admin);
        }

        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var admin = await _context.Admins
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admin == null)
            {
                return NotFound();
            }

            if (admin != null)
            {
                contentVm.Id = admin.Id;
                contentVm.Name = admin.Name;
                contentVm.Email = admin.Email;
            }
            return View(contentVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name,Email")] Admin admin)
        {
            if (id != admin.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Admins.Update(admin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdminExists(admin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(admin);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Developer")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var admin = await _context.Admins.FindAsync(id);
            var user = _userManager.Users.Where(u => u.ProfileID == admin.Id)?.FirstOrDefault();
            if (admin != null && user != null)
            {  
                try
                {                   
                        user.ProfileID = Guid.Empty;
                        await _userManager.UpdateAsync(user);                                          
                  
                    _context.Admins.Remove(admin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdminExists(admin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(admin);
        }

        [Authorize(Roles = "Developer")]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            var admin = await _context.Admins
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admin == null)
            {
                return NotFound();
            }

            if (admin != null)
            {
                contentVm.Id = admin.Id;
                contentVm.Name = admin.Name;
                contentVm.Email = admin.Email;
            }
            return View(contentVm);
        }
        #endregion  

        #region Private methods
        private ContentViewModel GetMaintenanceContent()
        {
            var admins = _context.Admins;
            contentVm.Admins = admins != null && admins.Count() > 0 ? admins : null;
            return contentVm;
        }        

        private bool AdminExists(Guid id)
        {
            return _context.Admins.Any(e => e.Id == id);
        }
        #endregion
    }
}
