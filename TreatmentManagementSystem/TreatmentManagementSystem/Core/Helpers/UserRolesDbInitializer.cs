﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using TMS.Data.Data.Models;
using TreatmentManagementSystem.Core.Constants;

namespace TreatmentManagementSystem.Core.Helpers
{
    public static class UserRolesDbInitializer
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {        
            var roleManager = serviceProvider
                .GetRequiredService<RoleManager<IdentityRole>>();
            const string powerUserRoleName = TmsUser.Roles.Developer;
            string[] roleNames = { powerUserRoleName, TmsUser.Roles.Administrator, TmsUser.Roles.Doctor, TmsUser.Roles.Patient };
            IdentityResult result;
            bool powerUserRoleExist;

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    result = await roleManager
                    .CreateAsync(new IdentityRole(roleName));
                }
            }
            powerUserRoleExist = await roleManager.RoleExistsAsync(powerUserRoleName);
            if (powerUserRoleExist)
            {
                var userManager = serviceProvider
                    .GetRequiredService<UserManager<ApplicationUser>>();
                var config = serviceProvider
                    .GetRequiredService<IConfiguration>();
                var admin = await userManager
                    .FindByEmailAsync(config["AdminCredentials:Email"]);

                if (admin == null)
                {
                    admin = new ApplicationUser()
                    {
                        UserName = config["AdminCredentials:Email"],
                        Email = config["AdminCredentials:Email"],
                        EmailConfirmed = true
                    };

                    result = await userManager
                        .CreateAsync(admin, config["AdminCredentials:Password"]);
                    if (result.Succeeded)
                    {
                        result = await userManager
                            .AddToRoleAsync(admin, powerUserRoleName);                       
                    }
                }
            }
        }
    }
}