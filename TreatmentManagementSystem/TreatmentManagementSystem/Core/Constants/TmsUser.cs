﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TreatmentManagementSystem.Core.Constants
{
    public class TmsUser
    {
        public class Roles
        {
            public const string Developer = "Developer";
            public const string Administrator = "Administrator";
            public const string Doctor = "Doctor";
            public const string Patient = "Patient";
        }
    }
}
