﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TMS.Data.Data.Models;

namespace TreatmentManagementSystem.ViewModels
{
    public class MessageViewModel
    {

        private IEnumerable<Message> messages;
        private IEnumerable<Doctor> doctors;

        private IEnumerable<Patient> patients;
        public InputModel Input { get; set; }

        public string MemberName { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsRead { get; set; }
        public Guid DoctorID { get; set; }
        public Guid PatientID { get; set; }
        public Guid SenderID { get; set; }
        public int MessageCount { get; set; }

        public IEnumerable<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        public IEnumerable<Patient> Patients
        {
            get { return patients; }
            set { patients = value; }
        }
        public IEnumerable<Doctor> Doctors
        {
            get { return doctors; }
            set { doctors = value; }
        }

        public class InputModel
        {
            [Required]
            public string Subject { get; set; }
            [Required]
            public string Text { get; set; }
            public bool IsRead { get; set; }
            [Required]
            public Guid DoctorID { get; set; }
            [Required]
            public Guid PatientID { get; set; }
            public Guid SenderID { get; set; }
        }
    }
}
