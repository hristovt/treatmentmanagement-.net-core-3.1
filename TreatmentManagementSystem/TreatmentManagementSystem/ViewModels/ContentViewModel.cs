﻿using System;
using System.Collections.Generic;
using TMS.Data.Data.Models;

namespace TreatmentManagementSystem.ViewModels
{
    public class ContentViewModel
    {
        private IEnumerable<Doctor> doctors;
        private IEnumerable<Patient> patients;
        private IEnumerable<Message> messages;
        private IEnumerable<Admin> admins;

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public string SearchString { get; set; }
        public bool IsHighAttention { get; set; }
        public int MessageCount { get; set; }

        public string UserId { get; set; }

        public IEnumerable<Doctor> Doctors
        {
            get { return doctors; }
            set { doctors = value; }
        }
        public IEnumerable<Patient> Patients
        {
            get { return patients; }
            set { patients = value; }
        }
        public IEnumerable<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        public IEnumerable<Admin> Admins
        {
            get { return admins; }
            set { admins = value; }
        }
    }
}
