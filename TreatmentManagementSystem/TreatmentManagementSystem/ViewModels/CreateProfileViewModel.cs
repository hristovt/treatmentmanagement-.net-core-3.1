﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TMS.Data.Data.Models;
using TreatmentManagementSystem.Models;

namespace TreatmentManagementSystem.ViewModels
{
    public class CreateProfileViewModel
    {
        private IQueryable<ApplicationUser> appUsers;
        
        public InputModel Input { get; set; }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public Doctor Doctor { get; set; }
        public IQueryable<ApplicationUser> AppUsers
        {
            get { return appUsers; }
            set { appUsers = value; }
        }

        public class InputModel
        {
            [Required]
            [Display(Name = "Name")]
            public string Name { get; set; }
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Display(Name = "Notes")]
            public string Notes { get; set; }

            [Display(Name = "With High Attention")]
            public bool WithHighAttention { get; set; }

            [Required]
            public string UserId { get; set; }
        }
    }
}
