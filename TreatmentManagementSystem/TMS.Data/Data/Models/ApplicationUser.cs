﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TMS.Data.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public Guid ProfileID { get; set; }
    }
}
