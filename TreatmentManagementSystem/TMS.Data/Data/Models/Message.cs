﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TMS.Data.Data.Models
{
    public class Message
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Text { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool IsRead { get; set; }
        public Guid DoctorID { get; set; }
        public Guid PatientID { get; set; }
        public Guid SenderID { get; set; }
    }
}
